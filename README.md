Gradle Kotlin build file example [![build status](https://gitlab.com/tomavenel/gradle_kotlin-example/badges/master/build.svg)](https://gitlab.com/tomavenel/gradle_kotlin-example/commits/master)
==================================================================================================================================================================================================

An example `build.gradle.kts` build script written in Kotlin to configure a Gradle project.
Adds many common plugins for the JVM (Java / Groovy / Kotlin / Junit / Spock / Findbugs / Jacoco / ...)


Usage :
`$ gradle wrapper && ./gradlew idea && ./gradlew build`


Have a look to `build.gradle.kts` for more information
Results in `<workspace>/src/build/reports/buildDashboard/index.html`
