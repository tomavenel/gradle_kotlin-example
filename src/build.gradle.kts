import org.gradle.api.plugins.quality.*
import org.gradle.api.reporting.dependencies.HtmlDependencyReportTask
import org.gradle.api.tasks.Exec
import org.gradle.api.tasks.javadoc.Groovydoc
import org.gradle.api.tasks.javadoc.Javadoc
import org.gradle.api.tasks.wrapper.Wrapper
import org.gradle.testing.jacoco.tasks.JacocoReport
import org.jetbrains.dokka.gradle.DokkaTask

buildscript {

    repositories {
        gradleScriptKotlin()
    }

    dependencies {
        classpath(kotlinModule("gradle-plugin"))
        classpath("org.jetbrains.dokka:dokka-gradle-plugin:0.9.13") // Kotlin doc
    }
}

plugins {
    id("com.gradle.build-scan") version "1.8"
    `build-dashboard`
    `project-report`
    `build-announcements` // sends build notifications to desktop
}

/*
buildScan {
    setLicenseAgreementUrl("https://gradle.com/terms-of-service")
    setLicenseAgree("yes")
    publishAlways()
}
*/

val resourcesDir = File(rootProject.projectDir.parentFile, "config")

subprojects {

    apply {
        plugin("kotlin")
        plugin("groovy") // for Spock tests
        plugin("jacoco")
        plugin("idea")
        plugin("eclipse")
        plugin("org.jetbrains.dokka")
        //TODO plugin("findbugs") (disable on tests)
        plugin("codenarc")
        plugin("checkstyle")
        plugin("pmd")
        plugin("jdepend")
    }

    repositories {
        jcenter()
        gradleScriptKotlin()
    }

    dependencies {
        compile("com.github.kittinunf.result:result:1.1.0") //Result kotlin
        testCompile("org.spockframework:spock-core:1.1-groovy-2.4") //Spock
        testCompile("com.athaydes:spock-reports:1.3.0") // better Spock reports (supports documentation annotations)
    }

    tasks {
        "dokkaJavadoc"(type = org.jetbrains.dokka.gradle.DokkaTask::class) {
            outputFormat = "javadoc"
            outputDirectory = "$buildDir/javadoc"
        }

        "dokkaMarkdown"(type = org.jetbrains.dokka.gradle.DokkaTask::class) {
            outputFormat = "markdown"
            outputDirectory = "$buildDir/dokkaMarkdown"
        }

        withType<CodeNarc> {
            configFile = File(File(resourcesDir, "codenarc"), "StarterRuleSet-AllRulesByCategory.groovy")
        }

        withType<Checkstyle> {
            val checkstyleDir = File(resourcesDir, "checkstyle")
            configFile = File(checkstyleDir, "google_checks-revised.xml")
            reports {
                xml.isEnabled = false
                html.isEnabled = true
            }
        }

        withType<FindBugs> {
            effort = "max" // find max bugs
            reportLevel = "low" // report all bugs
            excludeFilter = File(File(resourcesDir, "findbugs"), "excludeFilter.xml")
            reports {
                xml.isEnabled = false
                html.isEnabled = true
            }
        }

        "doc" {
            dependsOn(
                    tasks.withType(Javadoc::class.java),
                    tasks.withType(Groovydoc::class.java),
                    tasks.withType(DokkaTask::class.java),
                    tasks.withType(JacocoReport::class.java)
            )
        }

        "fullBuild" {
            dependsOn(
                    tasks.getByName("doc")
            )
        }
    }

}

tasks {
    "wrapper"(type = Wrapper::class) {
        gradleVersion = "4.0.1"
    }

    withType<HtmlDependencyReportTask> {
        projects = project.allprojects // activate dependency reports on all projects
    }

    "doc" {
        dependsOn(
                tasks.getByName("projectReport")
        )
    }

    "docgen"(type = Exec::class) {
        setExecutable("docgen") //TODO : check installed
        val outputDir = File(buildDir, "docgen")
        setArgs(listOf( "run", "-i", File(projectDir.parentFile, "docgen"), "-o", outputDir ))
        doLast { println("Documentation generated at ${File(outputDir, "index.html")}") }
    }

    "fullBuild" {
        dependsOn(
                tasks.getByName("doc"),
                tasks.getByName("docgen") //TODO : exec after ? (not depends on ?)
                //TODO tasks.getByPath(":build"),
        )
    }
}
