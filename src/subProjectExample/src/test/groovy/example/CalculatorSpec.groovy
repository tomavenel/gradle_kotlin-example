package example

import spock.lang.Specification

/**
 * Specifications for the Calculator.
 */
class CalculatorSpec extends Specification {

    def "Check calculator addition"() {
        given:"A calculator"
        def calc = new Calculator()
        when:"I sum 1 and 2"
        def result = calc.sum(1, 2)
        then:"The result must be 3"
        result == 3
    }
}
